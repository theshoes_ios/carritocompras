/**
 * Created by poli_ on 18/8/2017.
 */
module.exports = function (req, res, next) {
    if (req.session.credencialSegura) {
        return next();
    }
    res.status(500);
    return res.send('No está autorizado');
};
