/**
 * Created by poli_ on 18/8/2017.
 */
module.exports = {
    CrearUsuario: function (req, res) {
        if (req.method == 'POST') {
            var parametros = req.allParams();
            if (parametros.usuario && parametros.password) {
                var usuarioCrear = {
                    usuario: parametros.usuario,
                    password: parametros.password
                };
                Usuario.create(usuarioCrear).exec(function (err, usuarioCreado) {
                    if (err) {
                        res.status(500);
                        return res.send('Error al crear el usuario');
                    }
                    return res.ok('Usuario Creado');
                });
            }
            else {
                res.status(500);
                return res.send('No se completaron los campos');
            }
        }
        else {
            res.status(500);
            return res.send('Método incorrecto');
        }
    }
};
