/**
 * Created by poli_ on 18/8/2017.
 */
declare let module;
declare let Usuario;
declare let Comic;
declare let require;
declare let sails;

module.exports = {
  homepage: (req, res) => {
    return res.view('homepage');
  },

  login: (req, res) => {
    return res.view('usuario/login');
  },

  crearUsuario: (req, res) => {
    return res.view('usuario/crearUsuario');
  },

  comprarComics: (req, res) => {
    return res.view('compras/comprarComics');
  },

  libreriaCompras: (req, res) => {
    return res.view('compras/libreriaCompras');
  },

  acercaDe: (req, res) => {
    return res.view('aboutUs');
  }
};
