module.exports = {
    homepage: function (req, res) {
        return res.view('homepage');
    },
    login: function (req, res) {
        return res.view('usuario/login');
    },
    crearUsuario: function (req, res) {
        return res.view('usuario/crearUsuario');
    },
    comprarComics: function (req, res) {
        return res.view('compras/comprarComics');
    },
    libreriaCompras: function (req, res) {
        return res.view('compras/libreriaCompras');
    },
    acercaDe: function (req, res) {
        return res.view('aboutUs');
    }
};
