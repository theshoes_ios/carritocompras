/**
 * Created by poli_ on 18/8/2017.
 */
var Passwords = require('machinepack-passwords');
module.exports = {
    Login: function (req, res) {
        var parametros = req.allParams();
        if (parametros.usuario && parametros.password) {
            Usuario.findOne({
                usuario: parametros.usuario
            }).populate('comics').exec(function (err, usuarioEncontrado) {
                if (err) {
                    res.status(500);
                    return res.send('Problema con el login');
                }
                if (usuarioEncontrado) {
                    Passwords.checkPassword({
                        passwordAttempt: parametros.password,
                        encryptedPassword: usuarioEncontrado.password,
                    }).exec({
                        error: function (err) {
                            res.status(500);
                            return res.send('Problema con el login');
                        },
                        incorrect: function () {
                            res.status(500);
                            return res.send('Password incorrecto');
                        },
                        success: function () {
                            req.session.credencialSegura = usuarioEncontrado;
                            return res.ok(usuarioEncontrado);
                        },
                    });
                }
                else {
                    res.status(500);
                    return res.send('No existe el usuario');
                }
            });
        }
        else {
            res.status(500);
            return res.send('Necesitamos su nombre de usuario y password');
        }
    },
    Logout: function (req, res) {
        req.session.credencialSegura = undefined;
        return res.ok('Sesión cerrada');
    }
};
