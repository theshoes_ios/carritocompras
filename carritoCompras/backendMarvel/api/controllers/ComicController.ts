/**
 * Created by poli_ on 18/8/2017.
 */
module.exports = {
  CrearComic: function (req, res) {
    let params = req.allParams();

    if (params.id && params.title && params.series && params.description && params.price && params.pathImage) {

      let comicCarrito;

      if (params.id_user) {
        comicCarrito = {
          comicID: params.id,
          title: params.title,
          series: params.series,
          description: params.description,
          price: params.price,
          pathImage: params.pathImage,
          id_user: params.id_user
        };
      } else {
        comicCarrito = {
          comicID: params.id,
          title: params.title,
          series: params.series,
          description: params.description,
          price: params.price,
          pathImage: params.pathImage,
          id_user: req.session.credencialSegura.id
        };
      }

      Comic.create(comicCarrito).exec(function (err, comicAdd) {
        if (err) {
          res.status(500);
          return res.send('Error al añadir el comic');
        }

        return res.ok('Comic Añadido');
      });
    } else {
      res.status(500);
      return res.send('No se completaron los campos');
    }
  },

  CargarComics: function (req, res) {
    if (req.session.credencialSegura.id) {
      Comic.find({id_user: req.session.credencialSegura.id}).exec(function (err, comicsEncontrados) {
        if (err) {
          res.status(500);
          return res.send('Error al cargar comics');
        }
        return res.ok(comicsEncontrados)
      });
    } else {
      res.status(500);
      return res.send('No se envió el id de usuario');
    }
  }
};
