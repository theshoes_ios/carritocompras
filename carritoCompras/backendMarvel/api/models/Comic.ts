/**
 * Created by poli_ on 18/8/2017.
 */
module.exports = {

  attributes: {

    comicID: {
      type: 'string',
      required: true
    },
    title:{
      type: 'string',
      required: true
    },
    series:{
      type: 'string',
      required: true
    },
    description:{
      type: 'string',
      required: true
    },
    price:{
      type: 'string',
      required: true
    },
    pathImage:{
      type: 'string',
      required: true
    },
    id_user: {
      model: 'usuario',
      required: true
    }

  }
};
