/**
 * Created by poli_ on 18/8/2017.
 */
var Passwords = require('machinepack-passwords');
module.exports = {
    attributes: {
        usuario: {
            type: 'string',
            required: true,
            unique: true
        },
        password: {
            type: 'string',
            defaultsTo: '123456',
            required: true
        },
        comics: {
            collection: 'comic',
            via: 'id_user'
        }
    },
    beforeCreate: function (values, cb) {
        Passwords.encryptPassword({
            password: values.password
        }).exec({
            error: function (err) {
                cb(err);
            },
            success: function (result) {
                values.password = result;
                cb();
            }
        });
    }
};
