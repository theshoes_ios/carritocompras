//
//  TableSafeViewController.swift
//  carritoCompras
//
//  Created by Andrés Guerra Vásconez on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import PopupDialog
import Toast_Swift

class TableSafeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var TableSafe: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sm = SailsManager()
        sm.obtenerComics()
        
        NotificationCenter.default.addObserver(self, selector: #selector(saving), name: NSNotification.Name("saving"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saving () {
        TableSafe.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Saved Comics"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comicSaved.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "safeCell") as! SafeTableViewCell
        
        cell.comic = comicSaved[indexPath.row]
        cell.fillData()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = "Description"
        let message = comicSaved[indexPath.row]["descripcion"] as! String
        
        let popup = PopupDialog(title: title, message: message)
        
        let okButton = CancelButton(title: "OK") {
            print("OK")
        }
        
        popup.addButton(okButton)
        
        self.present(popup, animated: true, completion: nil)
    }

}
