//
//  SafeTableViewCell.swift
//  carritoCompras
//
//  Created by Andrés Guerra Vásconez on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class SafeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var seriesLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var imageComic: UIImageView!
    var comic: NSDictionary!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fillData() {
        titleLabel.text = "\(comic["title"]!)"
        seriesLabel.text = "\(comic["series"]!)"
        priceLabel.text = "\(comic["price"]!)"
        
        let bm = BackendManager()
        bm.getImageComic((comic["pathImage"]! as! String), completionHandler: { (imageR) in
            DispatchQueue.main.async {
                self.imageComic.image = imageR
            }
        })
    }
}
