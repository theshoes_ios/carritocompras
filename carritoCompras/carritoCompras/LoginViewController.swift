//
//  LoginViewController.swift
//  carritoCompras
//
//  Created by Patricio Chavez on 8/18/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UITextField!
    @IBOutlet weak var passwordLabel: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logInButtonPressed(_ sender: Any) {
        let sm = SailsManager()
        sm.login(user: usernameLabel.text!, pass: passwordLabel.text!){ (respuesta) in
            DispatchQueue.main.async {

                if (respuesta == "Ingreso Correcto") {
                    self.performSegue(withIdentifier: "authSegue", sender: self)
                }else{
                    let alert = UIAlertController(title: "Alert", message: "\(respuesta)", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Accept", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        usernameLabel.resignFirstResponder()
        passwordLabel.resignFirstResponder()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "authSegue" {
            let destinationController = segue.destination as! TabBarViewController
        }
        
    }
}
