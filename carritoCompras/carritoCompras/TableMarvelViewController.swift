//
//  TableMarvelViewController.swift
//  carritoCompras
//
//  Created by Patricio Chavez on 6/12/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import PopupDialog

class TableMarvelViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableComic: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let bm = BackendManager()
        bm.getAllComics()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name("reload"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(replicate), name: NSNotification.Name("replicate"), object: nil)
    }

    func reload() {
        tableComic.reloadData()
    }
    
    func replicate () {
        self.view.makeToast("This comic is already saved", duration: 3, position: .center)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let nameOfMonth = dateFormatter.string(from: now)
        
        return "Comics of " + nameOfMonth
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comicArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "comicCell") as! ComicTableViewCell
        
        
        cell.comic = comicArray[indexPath.row]
        cell.fillData()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = comicArray[indexPath.row].title
        let description = comicArray[indexPath.row].description ?? "No description"
        let series = comicArray[indexPath.row].series ?? "No series"
        let price = NSString(format: "%.2f", (comicArray[indexPath.row].prices?[0].price!)!)
        let message1 = "Description: " + description + "\n\n"
        let message2 = "Series: " + series + "\n\n"
        let message3 = "Price: " + (price as String)
        
        let popup = PopupDialog(title: title, message: message1 + message2 + message3)
        
        let cancelButton = CancelButton(title: "Cancel") {
            print("Closing")
        }
        
        let safeButton = DefaultButton(title: "Guardar") {
            let sm = SailsManager()
            let saved: Comic! = comicArray[indexPath.row]
            
            sm.guardarComic(saved)
            
            print("Guardando")
        }
        
        popup.addButtons([cancelButton, safeButton])
        
        self.present(popup, animated: true, completion: nil)
    }
}
