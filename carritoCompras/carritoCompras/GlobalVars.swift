//
//  GlobalVars.swift
//  carritoCompras
//
//  Created by Patricio Chavez on 6/12/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation


var comicArray = [Comic]()
var comicSaved = [NSDictionary]()

var contador:Int? {
    
    didSet{
        
        if contador == 0 {
            NotificationCenter.default.post(name: NSNotification.Name("reload"), object: nil)
        }
    }
    
}
