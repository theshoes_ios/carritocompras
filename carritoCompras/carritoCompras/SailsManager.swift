//
//  SailsManager.swift
//  carritoCompras
//
//  Created by Andrés Guerra Vásconez on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

class SailsManager {
    let urlM = "http://localhost:1337/Comic"
    
    func guardarComic (_ comicG: Comic) {
        
        let parametros: Parameters = [
            "comicID": comicG.id ?? "No id",
            "title": comicG.title ?? "No title",
            "series": comicG.series ?? "No series",
            "descripcion": comicG.description ?? "No description",
            "price": NSString(format: "%.2f", comicG.prices![0].price!),
            "pathImage": comicG.pathImage ?? "No image"
        ]
            
        Alamofire.request(urlM, method: .post, parameters: parametros).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                if let json = response.result.value {
                    let jsonDictionary = json as! NSDictionary
                    comicSaved += [jsonDictionary]
                    NotificationCenter.default.post(name: NSNotification.Name("saving"), object: nil)
                }
            case .failure:
                NotificationCenter.default.post(name: NSNotification.Name("replicate"), object: nil)
            }
        }
    }
    
    func obtenerComics () {
        
        comicSaved.removeAll()
        
        Alamofire.request(urlM).responseJSON { response in
            
            if let json = response.result.value {
                
                let jsonArray = json as! NSArray
                
                for comic in jsonArray {
                    let comicDic = comic as! NSDictionary
                    comicSaved += [comicDic]
                }
                NotificationCenter.default.post(name: NSNotification.Name("saving"), object: nil)
            }
        }
    }
    
    func login(user:String,pass:String, completionHandler: @escaping (String)->()) {
        Alamofire.request("http://localhost:1337/Auth/Login?usuario=\(user)&password=\(pass)", method: .post).responseString { response in

                completionHandler(response.value!)

            
        }
    }
}
