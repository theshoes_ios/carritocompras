//
//  ObjectMapper.swift
//  carritoCompras
//
//  Created by Patricio Chavez on 6/12/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import ObjectMapper

class ComicApiResponse:Mappable {
    var resultados:[Comic]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        resultados <- map["data.results"]
    }
}

class Comic:Mappable {
    var id:Int?
    var title:String?
    var series:String?
    var description:String?
    var thumbnail:UIImage?
    var prices:[PriceComic]?
    var price:String?
    var pathImage:String?
    
    required init(map:Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        description <- map["description"]
        series <- map["series.name"]
        prices <- map["prices"]
        price <- map["price"]
        pathImage <- map["thumbnail.path"]
    }
}

class PriceComic:Mappable {
    var type:String?
    var price:Float?
    
    required init(map:Map) {
        
    }
    
    func mapping(map: Map) {
        type <- map["type"]
        price <- map["price"]
    }
}
