//
//  SignUpViewController.swift
//  carritoCompras
//
//  Created by Patricio Chavez on 8/18/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var userLabel: UITextField!
    @IBOutlet weak var passLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        userLabel.resignFirstResponder()
        passLabel.resignFirstResponder()
    }

 

}
