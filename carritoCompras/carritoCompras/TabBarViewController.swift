//
//  TabBarViewController.swift
//  carritoCompras
//
//  Created by Andrés Guerra Vásconez on 15/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tabBar.tintColor = UIColor(red:0.71, green:0.04, blue:0.04, alpha:1.0)
    }

}
