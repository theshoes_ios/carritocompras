//
//  ComicTableViewCell.swift
//  carritoCompras
//
//  Created by Patricio Chavez on 6/12/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ComicTableViewCell: UITableViewCell {

    @IBOutlet weak var TituloLabel: UILabel!
    @IBOutlet weak var SerieLabel: UILabel!
    @IBOutlet weak var ComicImage: UIImageView!
    var comic: Comic!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(){
        
        TituloLabel.text = "\(comic.title!)"
        SerieLabel.text = "\(comic.series ?? "")"
        
        if comic.thumbnail == nil {
            
            let bm = BackendManager()
            
            bm.getImageComic((comic?.pathImage)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.ComicImage.image = imageR
                    self.comic.thumbnail = imageR
                }
                
            })
            
        } else {
            
            ComicImage.image = comic.thumbnail
            
        }
        
    }
    

}
