//
//  BackendManager.swift
//  carritoCompras
//
//  Created by Patricio Chavez on 6/12/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper


class BackendManager {
    
    let url = "https://gateway.marvel.com:443/v1/public/comics"
    
    let hash = "?dateDescriptor=thisMonth&ts=1&apikey=d1cd8f7c15025f9d58010b75312dab09&hash=937d6e8ecc37bc5706ad99a46ed08aa5"
    
    func getAllComics () {
        
        let urlC = url + hash
        
        Alamofire.request(urlC).responseObject { (response: DataResponse<ComicApiResponse>) in
            
            let comicResponse = response.result.value
            
            if let comicArrayAux = comicResponse?.resultados {
                
                contador = comicArrayAux.count
                // comicArray = comicArrayAux
                
                for comic in comicArrayAux {
                    comicArray += [comic]
                    contador = contador! - 1
                }
                
            }
        }
    }
    
    func getImageComic(_ path: String, completionHandler: @escaping(UIImage)->()){
        
        let index = path.index(path.startIndex, offsetBy: 4)
        
        let urlC = "https" + path.substring(from: index) + "/detail.jpg"
        
        Alamofire.request(urlC).responseImage { response in
            if let image = response.result.value {
                
                
                completionHandler(image)
            }
        }
        
    }
}
